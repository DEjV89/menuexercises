package pl.football.menuexcercise1;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.internal.view.menu.SubMenuBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

/**
 * Created by dkrezel on 2015-05-01.
 */
public class MainActivity extends ActionBarActivity {

	private CheckBox mCheckBox1;
	private CheckBox mCheckBox2;
	private CheckBox mCheckBox3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);

		prepareCheckboxListeners();
	}

	private void prepareCheckboxListeners() {
		mCheckBox1 = (CheckBox) findViewById(R.id.checkbox_1);
		mCheckBox2 = (CheckBox) findViewById(R.id.checkbox_2);
		mCheckBox3 = (CheckBox) findViewById(R.id.checkbox_3);

		mCheckBox1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// jeśli kliknięto checkbox, zmień pozycje w menu
				invalidateOptionsMenu();
			}
		});
		mCheckBox2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// jeśli kliknięto checkbox, zmień pozycje w menu
				invalidateOptionsMenu();
			}
		});
		mCheckBox3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// jeśli kliknięto checkbox, zmień pozycje w menu
				invalidateOptionsMenu();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.menu_item_1) {
			Toast.makeText(this, "Using Settings....", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (itemId == R.id.menu_item_2) {
			Toast.makeText(this, "Using Enable....", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (itemId == R.id.menu_item_3) {
			Toast.makeText(this, "Using Map....", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (itemId == R.id.menu_item_4) {
			Toast.makeText(this, "Using Submenu....", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (itemId == R.id.menu_radio_1) {
			Toast.makeText(this, "Using Red....", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (itemId == R.id.menu_radio_2) {
			Toast.makeText(this, "Using Green....", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (itemId == R.id.menu_radio_3) {
			Toast.makeText(this, "Using Blue....", Toast.LENGTH_SHORT).show();
			return true;
		}
		return true;
	}


	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// zmiana elementów Menu
		if (mCheckBox1.isChecked() ) {
			MenuItem mMainItem = menu.findItem(R.id.menu_id);
			SubMenu mSubMenu = mMainItem.getSubMenu();
			MenuItem mItem = mSubMenu.findItem(R.id.menu_item_1);
			mSubMenu.removeItem(mItem.getItemId());
		}
		if (mCheckBox2.isChecked() ) {
			MenuItem mMainItem = menu.findItem(R.id.menu_id);
			SubMenu mSubMenu = mMainItem.getSubMenu();
			MenuItem mItem = mSubMenu.findItem(R.id.menu_item_3);
			mSubMenu.removeItem(mItem.getItemId());
		}
		if (mCheckBox3.isChecked() ) {
			MenuItem mMainItem = menu.findItem(R.id.menu_id);
			SubMenu mSubMenu = mMainItem.getSubMenu();
			mSubMenu.removeGroup(R.id.menu_radio_group);
		}
			return super.onPrepareOptionsMenu(menu);
	}
}
